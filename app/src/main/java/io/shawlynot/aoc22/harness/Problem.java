package io.shawlynot.aoc22.harness;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.Charset;

public abstract class Problem {

    private final int number;

    public Problem(int number) {
        this.number = number;
    }

    private String getPuzzleInput() throws IOException {
        return IOUtils.resourceToString("/io/shawlynot/aoc-%d.txt".formatted(number), Charset.defaultCharset());
    }

    public final void solve() throws IOException {
        String puzzle = getPuzzleInput();
        String basicAnswer = basic(puzzle);
        String bonusAnswer = bonus(puzzle);
        System.out.printf("Answer to %d basic problem is %s\n", number, basicAnswer);
        System.out.printf("Answer to %d bonus problem is %s\n", number, bonusAnswer);
    }

    protected abstract String basic(String input);
    protected abstract String bonus(String input);

}
