package io.shawlynot.aoc22.puzzles;

import io.shawlynot.aoc22.harness.Problem;

import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class Aoc5 extends Problem {

    public Aoc5() {
        super(5);
    }

    @Override
    public String basic(String input) {
        var split = input.split("\n\n");
        Map<Integer, Deque<String>> towers = parseTowers(split[0]);
        List<Instruction> instructions = parseInstructions(split[1]);
        for (var instruction : instructions) {
            applyInstruction(instruction, towers);
        }
        return getTopOfTowers(towers);
    }

    @Override
    public String bonus(String input) {
        var split = input.split("\n\n");
        Map<Integer, Deque<String>> towers = parseTowers(split[0]);
        List<Instruction> instructions = parseInstructions(split[1]);
        for (var instruction : instructions) {
            applyInstructionBonus(instruction, towers);
        }
        return getTopOfTowers(towers);
    }

    private Map<Integer, Deque<String>> parseTowers(String towersStr) {
        var towersLn = towersStr.split("\n");
        var towersArr = Arrays.copyOfRange(towersLn, 0, towersLn.length - 1);
        Map<Integer, Deque<String>> towers = new HashMap<>();
        var parsedTowers = Arrays.stream(towersArr)
                .map(this::parseTowerRow).toList();
        for (int i = parsedTowers.size() - 1; i >= 0; i--) {
            var row = parsedTowers.get(i);
            for (int j = 0; j < row.size(); j++) {
                String item = row.get(j);
                if (!Objects.equals(item, " ")) {
                    towers.computeIfAbsent(j + 1, k -> new LinkedList<>()).push(item);
                }
            }
        }
        return towers;
    }

    private List<String> parseTowerRow(String s) {
        var out = new ArrayList<String>();
        for (int i = 0; i < s.length(); i += 4) {
            var nextItem = s.substring(i + 1, i + 2);
            out.add(nextItem);
        }
        while (out.size() < 9) {
            out.add(" ");
        }
        return out;
    }

    private List<Instruction> parseInstructions(String instructionsStr) {
        var lines = instructionsStr.split("\n");
        return Arrays.stream(lines)
                .map(l -> Pattern.compile("\\d+")
                        .matcher(l)
                        .results()
                        .map(MatchResult::group)
                        .map(Integer::parseInt)
                        .toList()
                ).map(matches ->
                        new Instruction(matches.get(0), matches.get(1), matches.get(2))
                ).toList();
    }

    private void applyInstruction(Instruction instruction, Map<Integer, Deque<String>> towers) {
        for (int i = instruction.count; i > 0; i--) {
            var from = towers.get(instruction.from).pop();
            towers.get(instruction.to).push(from);
        }
    }

    private void applyInstructionBonus(Instruction instruction, Map<Integer, Deque<String>> towers) {
        Deque<String> toTransfer = new LinkedList<>();
        for (int i = instruction.count; i > 0; i--) {
            toTransfer.push(towers.get(instruction.from).pop());
        }
        for (int i = instruction.count; i > 0; i--) {
            towers.get(instruction.to).push(toTransfer.pop());
        }
    }

    private String getTopOfTowers(Map<Integer, Deque<String>> towers) {
        StringBuilder out = new StringBuilder();
        for (var towerNo : towers.keySet()) {
            out.append(towers.get(towerNo).peek());
        }
        return out.toString();
    }

    public record Instruction(int count, int from, int to) {

    }
}
