package io.shawlynot.aoc22.puzzles;

import io.shawlynot.aoc22.harness.Problem;

public class Aoc6 extends Problem {

    public Aoc6() {
        super(6);
    }

    @Override
    protected String basic(String input) {
        int start = 0;
        int end = 3;
        while (end < input.length()) {
            var test = input.substring(start, end + 1);
            if (test.chars().distinct().count() == 4) {
                return String.valueOf(end + 1);
            }
            start++;
            end++;
        }
        return null;
    }

    @Override
    protected String bonus(String input) {
        int start = 0;
        int end = 13;
        while (end < input.length()) {
            var test = input.substring(start, end + 1);
            if (test.chars().distinct().count() == 14) {
                return String.valueOf(end + 1);
            }
            start++;
            end++;
        }
        return null;
    }
}
