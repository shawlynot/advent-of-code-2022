package io.shawlynot.aoc22.puzzles;

import io.shawlynot.aoc22.harness.Problem;

import java.util.HashMap;
import java.util.Map;

public class Aoc7 extends Problem {

    public Aoc7() {
        super(7);
    }

    @Override
    protected String basic(String input) {
        Dir root = generateFs(input);
        return String.valueOf(root.getSize(0).dirsLessThan1e5());
    }

    @Override
    protected String bonus(String input) {
        Dir root = generateFs(input);
        int totalUsed = root.getSize(0).thisNodeSize();
        int freeSpace = 70_000_000 - totalUsed;
        int spaceNeeded = 30_000_000 - freeSpace;
        return String.valueOf(root.getSize(spaceNeeded).closestAboveTarget());
    }

    private Dir generateFs(String input) {
        var lines = input.lines().toList();

        Dir root = new Dir(new HashMap<>(), null);
        Dir currentDir = root;
        for (int i = 0; i < lines.size(); i++) {
            //handle cd /
            String line = lines.get(i);
            if (line.equals("$ cd /") || line.equals("$ ls")) {
            } else if (line.equals("$ cd ..")) {
                currentDir = currentDir.parent();
            } else if (line.startsWith("$ cd ")) {
                var dir = line.split(" ")[2];
                currentDir = (Dir) currentDir.contents().get(dir);
            } else if (!line.startsWith("$")) {
                var split = line.split(" ");
                if (split[0].equals("dir")) {
                    currentDir.contents().put(split[1], new Dir(new HashMap<>(), currentDir));
                } else {
                    currentDir.contents().put(split[1], new File(Integer.parseInt(split[0]), currentDir));
                }
            } else {
                throw new IllegalStateException("this shouldn't have happened");
            }
        }
        return root;
    }

    private sealed interface Node {
        Size getSize(int target);
    }

    private record File(Integer size, Dir parent) implements Node {

        @Override
        public Size getSize(int target) {
            return new Size(size, 0, 0);
        }
    }

    private record Dir(Map<String, Node> contents, Dir parent) implements Node {

        @Override
        public Size getSize(int target) {
            int thisNodeSize = 0;
            int dirsBiggerThan1e5 = 0;
            int closestAboveTarget = Integer.MAX_VALUE;
            for (var node : contents.values()) {
                var contentSizes = node.getSize(target);
                thisNodeSize += contentSizes.thisNodeSize();
                dirsBiggerThan1e5 += contentSizes.dirsLessThan1e5();
                if (contentSizes.closestAboveTarget() > target && contentSizes.closestAboveTarget() < closestAboveTarget) {
                    closestAboveTarget = contentSizes.closestAboveTarget();
                }
            }
            if (thisNodeSize <= 1e5) {
                dirsBiggerThan1e5 += thisNodeSize;
            }
            if (thisNodeSize > target && thisNodeSize < closestAboveTarget){
                closestAboveTarget = thisNodeSize;
            }
            return new Size(thisNodeSize, dirsBiggerThan1e5, closestAboveTarget);
        }

        @Override
        public String toString() {
            return "[contents: " + String.join(",", contents.keySet()) + "]";
        }
    }

    private record Size(int thisNodeSize, int dirsLessThan1e5, int closestAboveTarget) {
    }

}