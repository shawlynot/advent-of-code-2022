package io.shawlynot.aoc22.puzzles;

import io.shawlynot.aoc22.harness.Problem;

import java.util.Arrays;

public class Aoc8 extends Problem {

    public Aoc8() {
        super(8);
    }

    @Override
    protected String basic(String input) {
        //int[row][column]
        Integer[][] lines = input.lines().map(l -> Arrays.stream(l.split("")).map(Integer::valueOf).toArray(Integer[]::new)).toArray(Integer[][]::new);
        boolean[][] above = getVisibleFromAbove(lines);
        boolean[][] below = getVisibleFromBelow(lines);
        boolean[][] fromLeft = getVisibleFromLeft(lines);
        boolean[][] fromRight = getVisibleFromRight(lines);

        int total = 0;
        for (int column = 0; column < lines[0].length; column++) {
            for (int row = 0; row < lines.length; row++) {
                if (above[row][column] || below[row][column] || fromLeft[row][column] || fromRight[row][column]) {
                    total++;
                }
            }
        }

        return String.valueOf(total);
    }

    private boolean[][] getVisibleFromAbove(Integer[][] lines) {
        boolean[][] out = new boolean[lines.length][lines[0].length];
        for (int column = 0; column < lines[0].length; column++) {
            int max = lines[0][column];
            out[0][column] = true;
            for (int row = 1; row < lines.length; row++) {
                var thisHeight = lines[row][column];
                out[row][column] = thisHeight > max;
                max = Math.max(thisHeight, max);
            }
        }
        return out;
    }

    private boolean[][] getVisibleFromBelow(Integer[][] lines) {
        boolean[][] out = new boolean[lines.length][lines[0].length];
        for (int column = 0; column < lines[0].length; column++) {
            int max = lines[lines.length - 1][column];
            out[lines.length - 1][column] = true;
            for (int row = lines.length - 2; row >= 0; row--) {
                var thisHeight = lines[row][column];
                out[row][column] = thisHeight > max;
                max = Math.max(thisHeight, max);
            }
        }
        return out;
    }

    private boolean[][] getVisibleFromLeft(Integer[][] lines) {
        boolean[][] out = new boolean[lines.length][lines[0].length];
        for (int row = 0; row < lines.length; row++) {
            int max = lines[row][0];
            out[row][0] = true;
            for (int column = 1; column < lines[0].length; column++) {
                var thisHeight = lines[row][column];
                out[row][column] = thisHeight > max;
                max = Math.max(thisHeight, max);
            }
        }
        return out;
    }

    private boolean[][] getVisibleFromRight(Integer[][] lines) {
        boolean[][] out = new boolean[lines.length][lines[0].length];
        for (int row = 0; row < lines.length; row++) {
            int max = lines[row][lines[0].length - 1];
            out[row][lines[0].length - 1] = true;
            for (int column = lines[0].length - 2; column >= 0; column--) {
                var thisHeight = lines[row][column];
                out[row][column] = thisHeight > max;
                max = Math.max(thisHeight, max);
            }
        }
        return out;
    }

    @Override
    protected String bonus(String input) {
        return null;
    }
}
